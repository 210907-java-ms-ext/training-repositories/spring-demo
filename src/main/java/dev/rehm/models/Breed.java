package dev.rehm.models;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class Breed {

    private int id;
    private String name;
    private int lifespan;
    private Size size;

    public Breed(){
        super();
    }

    public Breed(int id, String name, int lifespan, Size size) {
        this.id = id;
        this.name = name;
        this.lifespan = lifespan;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLifespan() {
        return lifespan;
    }

    public void setLifespan(int lifespan) {
        this.lifespan = lifespan;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Breed breed = (Breed) o;
        return id == breed.id && lifespan == breed.lifespan && Objects.equals(name, breed.name) && size == breed.size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lifespan, size);
    }

    @Override
    public String toString() {
        return "Breed{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lifespan=" + lifespan +
                ", size=" + size +
                '}';
    }
}
