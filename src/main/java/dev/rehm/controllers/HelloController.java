package dev.rehm.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller // spring core - register a spring bean, role is to handle requests
public class HelloController {

//    @GetMapping("/hello") //responds to GET request sent to /hello
    @RequestMapping(value="/hello", method= RequestMethod.GET)
    @ResponseBody //response bypasses the view resolver
    public String sayHi(@RequestParam(value = "name",required = false)String name){
        if(name==null){
            return "Hello World from my Spring Boot App!!!";
        } else {
            return "Hello "+ name+ "!! ";
        }
    }

}
