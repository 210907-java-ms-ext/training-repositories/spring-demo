package dev.rehm.models;

public enum Size {

    TOY, SMALL, MEDIUM, LARGE, GIANT
}
