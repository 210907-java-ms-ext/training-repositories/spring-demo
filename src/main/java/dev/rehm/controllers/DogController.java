package dev.rehm.controllers;

import dev.rehm.models.Dog;
import dev.rehm.services.DogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class DogController {

    /*
        GET     /dogs - get all dog records         localhost:8080/dogs
                optional request param: breed       localhost:8080/dogs?breed=pitbull
                .... etc
        GET     /dogs/{id} - get dog record by its id
        POST    /dogs - add a new dog record
        PUT     /dogs/{id} - update an exist dog record by its id
        DELETE  /dogs/{id} - delete a dog record by its id
     */

    @Autowired
    public DogService dogService;

    @GetMapping("/dogs")  // /dogs, /dogs?breed=...
    @ResponseBody
    public List<Dog> getAllDogs(@RequestParam(value = "breed", required = false)String breed, @RequestParam(value=
            "name", required = false)String name){
        if(breed==null){
            if(name==null){
                return dogService.getAllDogs();
            } else {
                return dogService.getDogsByName(name);
            }
        } else {
            if(name==null){
                return dogService.getDogsByBreedName(breed);
            } else {
                return dogService.getDogsByNameAndBreed(name, breed);
            }
        }
        // spring automatically converts this to json
    }

    @GetMapping("/dogs/{id}")
    @ResponseBody
    public Dog getDogById(@PathVariable("id")int id){
        return dogService.getDogById(id);
    }

    @PostMapping("/dogs")
    @ResponseBody
    public ResponseEntity<Dog> createNewDog(@RequestBody Dog dog){
        return new ResponseEntity<>(dogService.createNewDog(dog), HttpStatus.CREATED);
    }






}
